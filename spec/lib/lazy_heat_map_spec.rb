require 'spec_helper'
require 'lazy_heat_map'

RSpec.describe LazyHeatMap do
  subject { described_class.new(resources, policy_spec) }

  let(:resources) do
    [
      { labels: %w[P1 S2], web_url: 'https://gitlab.com/g/p/issues/1' },
      { labels: %w[P3 S3] },
      { labels: %w[P2] },
      { labels: %w[S3] }
    ]
  end

  let(:policy_spec) do
    { conditions: { state: 'open', labels: %w[bug Category] } }
  end

  describe '#to_s' do
    it 'generates the heatmap' do
      p1s2 = '[1](https://gitlab.com/g/p/issues?state=open&label_name%5B%5D=bug&label_name%5B%5D=Category&label_name%5B%5D=P1&label_name%5B%5D=S2)'
      p3s3 = '[1](https://gitlab.com/g/p/issues?state=open&label_name%5B%5D=bug&label_name%5B%5D=Category&label_name%5B%5D=P3&label_name%5B%5D=S3)'
      p2 = '[1](https://gitlab.com/g/p/issues?state=open&label_name%5B%5D=bug&label_name%5B%5D=Category&label_name%5B%5D=P2&label_name%5B%5D=%22No+severity%22)'
      s3 = '[1](https://gitlab.com/g/p/issues?state=open&label_name%5B%5D=bug&label_name%5B%5D=Category&label_name%5B%5D=%22No+priority%22&label_name%5B%5D=S3)'

      expect(subject.to_s).to eq(<<~MARKDOWN.chomp)
        || ~S1 | ~S2 | ~S3 | ~S4 | ~"No severity" |
        |----|----|----|----|----|----|
        | ~P1 | 0 | #{p1s2} | 0 | 0 | 0 |
        | ~P2 | 0 | 0 | 0 | 0 | #{p2} |
        | ~P3 | 0 | 0 | #{p3s3} | 0 | 0 |
        | ~P4 | 0 | 0 | 0 | 0 | 0 |
        | ~"No priority" | 0 | 0 | #{s3} | 0 | 0 |
      MARKDOWN
    end
  end
end
