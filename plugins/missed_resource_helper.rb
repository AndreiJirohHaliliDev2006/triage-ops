# frozen_string_literal: true

require_relative File.expand_path('../lib/missed_resource_helper.rb', __dir__)

Gitlab::Triage::Resource::Context.include MissedResourceHelper
