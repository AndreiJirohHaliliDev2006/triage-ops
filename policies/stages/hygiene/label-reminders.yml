.regression_missing_labels:
  - &regression_label_missing
    name: Label missed ~regression for `regression:xx.x`
    conditions:
      state: opened
      forbidden_labels:
        - regression
      labels:
        - regression:{11..13}.{0..15}
    actions:
      labels:
        - regression
        - bug

  - &bug_label_missing
    name: Label missed ~bug for ~regression
    conditions:
      state: opened
      forbidden_labels:
        - bug
      labels:
        - regression
    actions:
      labels:
        - bug

.broken_master_missing_labels:
  - &priority_severity_labels_missing_for_broken_master
    name: Severity/priority labels missing for ~"master:broken"
    conditions:
      state: opened
      date:
        attribute: created_at
        condition: newer_than
        interval_type: months
        interval: 1
      labels:
        - master:broken
      forbidden_labels:
        - S1
        - P1
    actions:
      labels:
        - S1
        - P1

resource_rules:
  issues:
    rules:
      - *regression_label_missing
      - *bug_label_missing
      - *priority_severity_labels_missing_for_broken_master

      - name: Remind team members to set relevant labels on their Issues
        conditions:
          author_member:
            source: group
            condition: member_of
            # gitlab-org
            source_id: 9970
          labels:
            - none
          state: opened
        actions:
          labels:
            - auto updated
          comment: |
            Hi {{author}},

            Please [add labels to your issue](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#labels), this aids categorization and locating issues in the future.

            Thanks for your help! :heart:

            ----

            You are welcome to help [improve this comment](https://gitlab.com/gitlab-org/quality/triage-ops/blob/master/policies/stages/hygiene/label-reminders.yml).

      - name: Label `regression:xx.x` for ~regression where xx.x is current version
        conditions:
          state: opened
          ruby: |
            resource[:labels].grep(/^regression:/).empty?
          labels:
            - regression
        actions:
          labels:
            - bug
          comment: |
            This issue has ~regression without specifying which milestone introduced it. We're assuming that it's introduced in the current version #{instance_version.version} so we're labeling ~"regression:#{instance_version.version_short}".

            Please correct it accordingly and keep ~regression so that we could easily search for all the regressions across version, too.

            /label ~"regression:#{instance_version.version_short}"

      - name: Categorize Geo issues
        conditions:
          state: opened
          labels:
            - Geo
          forbidden_labels:
            - Geo Verification/Accuracy
            - Geo Replication/Sync
            - Geo Performance
            - Geo Administration
            - Geo DR
            - UX
            - backstage
            - technical debt
            - auto updated
            - triage-package
        limits:
          most_recent: 20
        actions:
          labels:
            - auto updated
          comment: |
            Hi {{author}},

            This issue is labelled ~Geo and we would like some more information about it.

            Please add any of these labels to help categorize this issue:
            - ~"Geo Verification/Accuracy"
            - ~"Geo Replication/Sync"
            - ~"Geo Performance"
            - ~"Geo Administration"
            - ~"Geo DR"
            - ~"UX"
            - ~"backstage"
            - ~"technical debt"

  merge_requests:
    rules:
      - *regression_label_missing
      - *bug_label_missing
      - *priority_severity_labels_missing_for_broken_master

      - name: Remind team members to set relevant labels on their MRs
        conditions:
          author_member:
            source: group
            condition: member_of
            # gitlab-org
            source_id: 9970
          labels:
            - none
          state: opened
        actions:
          labels:
            - auto updated
          comment: |
            Hi {{author}},

            Please [add labels to your merge request](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#labels), this helps triage community merge requests.

            Thanks for your help! :heart:

            ----

            You are welcome to help [improve this comment](https://gitlab.com/gitlab-org/quality/triage-ops/blob/master/policies/stages/hygiene/label-reminders.yml).
